import React from 'react';
import { Navigator } from 'react-native';
import { Scene, Router, Modal } from 'react-native-router-flux';
import Login from './scenes/auth/Login';
import SignUp from './scenes/auth/SignUp';
import Home from './scenes/user/Home';
import BasicDetails from './scenes/user/BasicDetails';
import Profile from './scenes/user/Profile';
import Interests from './scenes/user/Interests';
import PentorDrawer from './components/PentorDrawer';
import NavigationIcons from './components/NavigationIcons';
import * as colors from './config/colors';
import SplashScreen from './scenes/SplashScreen';


const RouterComponent = () => {
  return (
    <Router
      barButtonIconStyle={{ tintColor: 'white' }}
      sceneStyle={{ paddingTop: 20 }}
    >
      <Scene hideNavBar key="root" component={SplashScreen} />
      <Scene hideNavBar key="signup" component={SignUp} title="SignUp" />
      <Scene hideNavBar key="login" component={Login} title="Login" />
      <Scene key="drawer" component={PentorDrawer} open={false} >
        <Scene
          key="user"
          sceneStyle={{ paddingTop: 50 }}
          navigationBarStyle={{ backgroundColor: colors.PENTOR_BLUE }}
          titleStyle={{ color: 'white' }}
        >
          <Scene
            key="home"
            component={Home}
            title="Home"
            sceneStyle={{ paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight }}
            renderLeftButton={NavigationIcons.hamburgerButton}
          />
          <Scene
            key="profile"
            component={Profile}
            title="Photo / Bio"
            sceneStyle={{ paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight }}
            renderLeftButton={NavigationIcons.backButton}
          />
          <Scene
            key="basicdetails"
            component={BasicDetails}
            title="Basic Details"
            sceneStyle={{ paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight }}
            renderLeftButton={NavigationIcons.backButton}
          />
            <Scene
              key="interests"
              component={Interests}
              title="Interests"
              sceneStyle={{ paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight }}
              renderLeftButton={NavigationIcons.backButton}
            />
            <Scene key="modal" component={Modal} >
              <Scene
                key="subinterests"
                component={Interests}
                title="Interests"
                direction="vertical"
                sceneStyle={{ paddingTop: Navigator.NavigationBar.Styles.General.TotalNavHeight }}
                renderLeftButton={NavigationIcons.backButton}
              />
          </Scene>
        </Scene>
      </Scene>
    </Router>
  );
};

export default RouterComponent;

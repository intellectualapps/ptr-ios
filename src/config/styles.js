/**** COMMON STYLES *********/
import * as colors from './colors';

export const pentorInput = {
  color: colors.PENTOR_BLUE,
  fontSize: 16
};

export const pentorLabel = {
  color: colors.PENTOR_BLUE
};

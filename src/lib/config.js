module.exports = {
  AUTH_DATA_KEY: 'AUTH_DATA_KEY',
  backend: {
    hapiRemote: false,
    hapiLocal: false,
    pentorRemote: true,
    pentorLocal: false
  },
  HAPI: {
    local: {
      url: 'http://localhost:5000'
    },
    remote: {
      url: 'https://snowflakeserver-bartonhammond.rhcloud.com/'
    }
  },
  PENTOR: {
    // appId: 'snowflake',                              // match APP_ID in pentor-server's index.js
    local: {
      url: 'http://localhost:1337/parse'             // match SERVER_URL in pentor-server's index.js
    },
    remote: {
      url: 'https://pentor-bff-ios.appspot.com/api/v1'   // match SERVER_URL in pentor-server's index.js
    }
  }
};

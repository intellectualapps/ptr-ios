/**
 * # BackendFactory
 *
 * This class sets up the backend by checking the config.js
 *
 */
'use strict';

import CONFIG from './config';
import { pentorBackend } from './PentorBackend';
// import {anotherBAckend} from './anotherBAckend'

export default function BackendFactory(token = null) {
  if (CONFIG.backend.pentorRemote || CONFIG.backend.pentorLocal) {
    pentorBackend.initialize(token);
    return pentorBackend;
  }/* else if (CONFIG.backend.parseLocal || CONFIG.backend.parseRemote) {
    parse.initialize(token)
    return parse
  } */
}

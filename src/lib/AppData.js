/**
 * # AppData.js
 *
 * provides interfaces to frequently accessed application data
 *
 * Singleton module see https://k94n.com/es6-modules-single-instance-pattern
 */
'use strict';
/**
 * ## Imports
 *
 * Redux  & the config file
 */
import { appAuthData } from './AppAuthData';

export class AppData {
  /**
   * ## AppAuthToken
   *
   * set the key from the config
   */
  constructor() {
      this.setAuthData();
  }

  /**
   * ## getAuthToken
   * returns Token
   */
  getAuthToken = () => {
      return this.authData.authToken;
  }

  /**
   * ## getEmail
   * returns Token
   */
  getEmail = () => {
      return this.authData.email;
  }

  /**
   * ## getFirstName
   * returns Token
   */
  getFirstName = () => {
      return this.authData.firstName;
  }

  /**
   * ## setFirstName
   */
  setFirstName = (firstName) => {
      this.authData.firstName = firstName;
  }

  /**
   * ## getLastName
   * returns Token
   */
  getLastName = () => {
      return this.authData.lastName;
  }

  /**
   * ## setFirstName
   */
  setLastName = (lastName) => {
      this.authData.lastName = lastName;
  }

  /**
   * ## getAuthData
   * returns application authData to be handled with promise
   */
  setAuthData() {
    appAuthData.getAuthData()
      .then((data) => {
        // only set it if data hasn't already bn set
        if ((this.authData === undefined) || (this.authData === null)) {
          this.authData = data;
        }
      });
  }
}

// The singleto n variable
export const appData = new AppData();

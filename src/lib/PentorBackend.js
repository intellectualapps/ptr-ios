/**
 * # PentorBackend.js
 *
 * This class interfaces with pentorBackend-server using the rest api
 * see [https://pentorBackendplatform.github.io/docs/rest/guide/]
 *
 */
'use strict';

/**
 * ## Imports
 *
 * Config for defaults and underscore for a couple of features
 */
import _ from 'underscore';
import CONFIG from './config';
import Backend from './Backend';

export class PentorBackend extends Backend {
  /**
   * ## PentorBackend.js client
   *
   *
   * @throws tokenMissing if token is undefined
   */
  initialize(token) {
    // if (!_.isNull(token) && _.isUndefined(token.sessionToken)) {
      // throw new Error('TokenMissing');
    // }

    this._sessionToken =
      _.isNull(token) ? null : token;

    // this._applicationId = CONFIG.PARSE.appId
    this.API_BASE_URL = CONFIG.backend.pentorLocal
    ? CONFIG.PENTOR.local.url
    : CONFIG.PENTOR.remote.url;
  }

  /**
   * ### signup
   *
   * @param data object
   *
   * {username: "barton", email: "foo@gmail.com", password: "Passw0rd!"}
   *
   * @return
   * if ok, res.json={createdAt: "2015-12-30T15:17:05.379Z",
   *   objectId: "5TgExo2wBA",
   *   sessionToken: "r:dEgdUkcs2ydMV9Y9mt8HcBrDM"}
   *
   * if error, {code: xxx, error: 'message'}
   */
  async signup(data, token = null) {
    if (token != null) {
      this._sessionToken = token;
    }

    let formBody = [];
    for (const [key, value] of Object.entries(data)) {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(value);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    return await this._fetch({
      method: 'POST',
      url: '/user',
      body: formBody
    })
      .then((res) => {
        return res.json().then((json) => {
          if (res.status === 200 || res.status === 201) {
            return json;
          } else {
            throw (json);
          }
        });
      })
      .catch((error) => {
        throw (error);
      });
  }
  /**
   * ### login
   * encode the data and and call _fetch
   *
   * @param data
   *
   *  {username: "barton", password: "Passw0rd!"}
   *
   * @returns
   *
   * createdAt: "2015-12-30T15:29:36.611Z"
   * updatedAt: "2015-12-30T16:08:50.419Z"
   * objectId: "Z4yvP19OeL"
   * email: "barton@foo.com"
   * sessionToken: "r:Kt9wXIBWD0dNijNIq2u5rRllW"
   * username: "barton"
   *
   */
  async login(data, token = null) {
    if (token != null) {
      this._sessionToken = token;
    }

    let formBody = [];
    for (const [key, value] of Object.entries(data)) {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(value);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    return await this._fetch({
      method: 'POST',
      url: '/user/authenticate',
      body: formBody
    })
      .then((res) => {
        return res.json().then((json) => {
          if (res.status === 200 || res.status === 201) {
            return json;
          } else {
            throw (json);
          }
        });
      })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### getUser
   * Using the sessionToken, we'll get everything about
   * the current user.
   *
   * @returns
   *
   * if good:
   * {createdAt: "2015-12-30T15:29:36.611Z"
   *  email: "barton@acclivyx.com"
   *  objectId: "Z4yvP19OeL"
   *  sessionToken: "r:uFeYONgIsZMPyxOWVJ6VqJGqv"
   *  updatedAt: "2015-12-30T15:29:36.611Z"
   *  username: "barton"}
   *
   * if error, {code: xxx, error: 'message'}
   */
  async getUser(email) {
    return await this._fetch({
      method: 'GET',
      url: '/user?email='.concat(email)
    })
     .then((response) => {
       return response.json().then((res) => {
         if ((response.status === 200 || response.status === 201)) {
           return res;
         } else {
           throw (res);
         }
       });
     })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### getUser
   * Using the sessionToken, we'll get everything about
   * the current user.
   *
   * @returns
   *
   * if good:
   * {createdAt: "2015-12-30T15:29:36.611Z"
   *  email: "barton@acclivyx.com"
   *  objectId: "Z4yvP19OeL"
   *  sessionToken: "r:uFeYONgIsZMPyxOWVJ6VqJGqv"
   *  updatedAt: "2015-12-30T15:29:36.611Z"
   *  username: "barton"}
   *
   * if error, {code: xxx, error: 'message'}
   */
  async saveUUID(email, uuid) {
    return await this._fetch({
      method: 'GET',
      url: '/user/device-token/' + email + '?token=' + this._sessionToken + '&uuid=' + uuid + '&platform=2'
    })
     .then((response) => {
       return response.json().then((res) => {
         if ((response.status === 200 || response.status === 201)) {
           return res;
         }
         throw (res);
       });
     })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### getAreasofInterests
   * fetches areas of interests
   *
   *
   * @returns array interests
   *
   * if error:  {code: xxx, error: 'message'}
   */
  async getAreasOfInterests() {
    return await this._fetch({
      method: 'GET',
      url: '/interest'
    })
     .then((response) => {
       return response.json().then((res) => {
         if ((response.status === 200 || response.status === 201)) {
           return res;
         } else {
           throw (res);
         }
       });
     })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### getSubInterests
   * fetches areas of interests
   *
   *
   * @returns array interests
   *
   * if error:  {code: xxx, error: 'message'}
   */
  async getSubInterests(id) {
    return await this._fetch({
      method: 'GET',
      url: '/interest?sub='.concat(id)
    })
     .then((response) => {
       return response.json().then((res) => {
         if ((response.status === 200 || response.status === 201)) {
           return res;
         } else {
           throw (res);
         }
       });
     })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### updateUser
   * for this user, update their record
   * the data is already in JSON format
   *
   * @param userId  _id of PentorBackend.com
   * @param data object:
   * {username: "barton", email: "barton@foo.com"}
   */
  async updateUser(data, token = null) {
    if (token != null) {
      this._sessionToken = token;
    }

    let formBody = [];
    for (const [key, value] of Object.entries(data)) {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(value);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    return await this._fetch({
      method: 'PUT',
      url: '/user/basic',
      body: formBody
    })
      .then((res) => {
        return res.json().then((json) => {
          if (res.status === 200 || res.status === 201) {
            return json;
          } else {
            throw (json);
          }
        });
      })
      .catch((error) => {
        throw (error);
      });
  }

  /**
   * ### logout
   * prepare the request and call _fetch
   */
  async logout() {
    return await this._fetch({
      method: 'POST',
      url: '/logout',
      body: {}
    })
      .then((res) => {
        if ((res.status === 200 ||
          res.status === 201 ||
          res.status === 400) ||
        (res.code === 209)) {
          return {}
        } else {
          throw new Error({code: 404, error: 'unknown error from PentorBackend.com'});
        }
      })
      .catch((error) => {
        throw (error)
      })
  }
  /**
   * ### resetPassword
   * the data is already in a JSON format, so call _fetch
   *
   * @param data
   * {email: "barton@foo.com"}
   *
   * @returns empty object
   *
   * if error:  {code: xxx, error: 'message'}
   */
  async resetPassword (data) {
    return await this._fetch({
      method: 'POST',
      url: '/requestPasswordReset',
      body: data
    })
      .then((res) => {
        return res.json().then(function (json) {
          if ((res.status === 200 || res.status === 201)) {
            return {};
          } else {
            throw (json);
          }
        })
      })
      .catch((error) => {
        throw (error);
      })
  }

  /**
   * ### _fetch
   * A generic function that prepares the request
   * @returns object:
   *  {code: response.code
   *   status: response.status
   *   json: reponse.json()
   */
  async _fetch (opts) {
    opts = _.extend({
      method: 'GET',
      url: null,
      body: null,
      callback: null
    }, opts)

    let reqOpts = {
      method: opts.method,
      headers: {
        // 'X-PentorBackend-Application-Id': this._applicationId,
        // 'X-PentorBackend-REST-API-Key': this._restAPIKey
      }
    };

    if (this._sessionToken) {
      reqOpts.headers['Authorization'] = 'Bearer ' + this._sessionToken;
    }

    if (opts.method === 'POST' || opts.method === 'PUT') {
      reqOpts.headers['Accept'] = 'application/json';
      reqOpts.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      // reqOpts.headers['Content-Type'] = 'application/json'
    }

    if (opts.body) {
      reqOpts.body = opts.body; // JSON.stringify(opts.body)
    }

    return await fetch(this.API_BASE_URL + opts.url, reqOpts);
  }
}
// The singleton variable
export const pentorBackend = new PentorBackend();

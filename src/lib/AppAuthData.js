/**
 * # AppAuthToken.js
 *
 * A thin wrapper over the react-native-simple-store
 *
 * Singleton module see https://k94n.com/es6-modules-single-instance-pattern
 */
'use strict';
/**
 * ## Imports
 *
 * Redux  & the config file
 */
import store from 'react-native-simple-store';
import CONFIG from './config';

export class AppAuthData {
  /**
   * ## AppAuthToken
   *
   * set the key from the config
   */
  constructor() {
    this.AUTH_DATA_KEY = CONFIG.AUTH_DATA_KEY;
  }

  /**
   * ### storeAuthData
   * Store the auth data key
   */
  storeAuthData(authData) {
    return store.save(this.AUTH_DATA_KEY, authData);
  }
  /**
   * ### getAuthData
   * @param {Object} authData the currentUser object
   *
   * When Hot Loading, the authData  will be passed in, and if so,
   * it needs to be stored on the device.  Remember, the store is a
   * promise so, have to be careful.
   */
  getAuthData(authData) {
    if (authData) {
      return store.save(this.AUTH_DATA_KEY, authData).then(() => store.get(this.AUTH_DATA_KEY));
    }
    return store.get(this.AUTH_DATA_KEY);
  }
  /**
   * ### deleteAuthData
   * Deleted during log out
   */
  deleteAuthData() {
    return store.delete(this.AUTH_DATA_KEY);
  }
}
// The singleton variable
export const appAuthData = new AppAuthData();

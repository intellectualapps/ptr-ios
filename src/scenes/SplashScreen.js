import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { appData } from '../lib/AppData';
import { addAuthData } from '../reducers/auth/AuthActions';


class SplashScreen extends Component {

  componentWillMount() {
    // time out is required for splash screen to display before routing to the appropriate scene
    // also required so promise returns 'appData.authData' before the of condition
    setTimeout(() => {
      if (appData.authData) {
        this.props.addAuthData(appData.authData);
        Actions.drawer();
        Actions.user();
        Actions.home();
      } else {
        Actions.signup();
      }
    }, 1000);
  }

  render() {
      return (
        <View style={styles.container}>
        <Image source={require('../images/logo.png')} />
      </View>
      );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2a7fff'
  }
};

export default connect(null, { addAuthData })(SplashScreen);

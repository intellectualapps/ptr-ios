import React, { Component } from 'react';
import { Icon, Container, Button, Label, Content, Item,
  Form, Text, Spinner, Input } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Image } from 'react-native';
import { clearError } from '../../reducers/global/GlobalActions';
import { login } from '../../reducers/auth/AuthActions';
import { FacebookButton, TwitterButton } from '../../components/SocialButtons';


class LoginForm extends Component {
  state = { email: '', password: '' };

  componentWillMount() {
     // use lifecycle method to get email amd password if already set in signup page
     this.state.email = this.props.email;
     this.state.password = this.props.password;
  }

  onButtonPress() {
    const { email, password } = this.state;
    this.props.login(email, password);
  }

  showSignupPage() {
    this.props.clearError();
    Actions.signup();
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info full onPress={this.onButtonPress.bind(this)}>
        <Text> Login </Text>
      </Button>
    );
  }

  render() {
      return (
        <Container>
          <Image
            source={require('../../images/register_bg.jpg')}
            style={styles.backgroundImage}
          >
            <Content>
              <Content >
                <Button transparent block onPress={() => this.showSignupPage()}>
                  <Icon name="ios-arrow-back" />
                  <Text> Back to SignUp page</Text>
                </Button>
              </Content>
              <Content style={{ paddingTop: 20 }}>
                <Form>
                  <Item floatingLabel>
                  <Label style={{ color: 'white' }}> Email </Label>
                    <Input
                      autoCorrect={false}
                      style={{ color: '#fff', fontSize: 13 }}
                      onFocus={() => this.props.clearError()}
                      onChangeText={(val) => this.setState({ email: val })}
                      value={this.state.email}
                    />
                  </Item>
                  <Item floatingLabel>
                    <Label style={{ color: 'white' }}> Password </Label>
                    <Input
                      style={{ color: '#fff' }}
                      onFocus={() => this.props.clearError()}
                      secureTextEntry
                      onChangeText={(val) => this.setState({ password: val })}
                      value={this.state.password}
                    />
                  </Item>
                </Form>
              </Content>

              <Text style={styles.errorTextStyles}>
                {this.props.error}
              </Text>

              <Content>
                {this.renderButton()}
              </Content>

              <Content style={styles.socaialButtons}>
                <Text style={styles.centerTextStyle}> or </Text>
                <FacebookButton label='Login with Facebook' auth={this.props.login} />
                <TwitterButton label='Login with Twitter' auth={this.props.login} />
              </Content>
            </Content>
          </Image>
        </Container>
      );
  }
}

const mapStateToProps = ({ global }) => {
  const { error, loading } = global;
  return { error, loading };
};

const styles = {
  errorTextStyles: {
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 17,
    alignSelf: 'center',
    color: 'red'
  },
  backgroundImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: null
  },
  rightTextStyle: {
    alignSelf: 'flex-end',
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  centerTextStyle: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 25,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  socaialButtons: {
    paddingTop: 20,
    alignSelf: 'center'
  }

};
export default connect(mapStateToProps, { login, clearError })(LoginForm);

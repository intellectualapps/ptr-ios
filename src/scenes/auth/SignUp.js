import React, { Component } from 'react';
import { Container, Button, Content, Item, Form, Text, Spinner, Input, Label } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Image } from 'react-native';
import { clearError } from '../../reducers/global/GlobalActions';
import { getSessionToken, signup } from '../../reducers/auth/AuthActions';
import { FacebookButton, TwitterButton } from '../../components/SocialButtons';


class SignUpForm extends Component {
  state = { email: '', password: '' };

  onButtonPress() {
    const { email, password } = this.state;
    this.props.signup(email, password);
  }

  showLoginPage() {
    const { email, password } = this.state;
    this.props.clearError();
    Actions.login({ email, password });
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info full onPress={this.onButtonPress.bind(this)}>
        <Text> SignUp </Text>
      </Button>
    );
  }

  render() {
      return (
        <Container>
          <Image
            source={require('../../images/register_bg.jpg')}
            style={styles.backgroundImage}
          >
            <Content>
              <Content>
                <Text
                  onPress={() => this.showLoginPage()}
                  style={styles.rightTextStyle}
                >
                  Login
                </Text>
              </Content>
              <Content>
                <Form>
                  <Item floatingLabel>
                  <Label style={{ color: 'white' }}> Email </Label>
                    <Input
                      autoCorrect={false}
                      style={{ color: '#fff', fontSize: 13 }}
                      onFocus={() => this.props.clearError()}
                      onChangeText={(val) => this.setState({ email: val })}
                      value={this.state.email}
                    />
                  </Item>
                  <Item floatingLabel>
                    <Label style={{ color: 'white' }}> Password </Label>
                    <Input
                      style={{ color: '#fff' }}
                      onFocus={() => this.props.clearError()}
                      secureTextEntry
                      onChangeText={(val) => this.setState({ password: val })}
                      value={this.state.password}
                    />
                  </Item>
                </Form>
              </Content>

              <Text style={styles.errorTextStyles}>
                {this.props.error}
              </Text>

              <Content>
                {this.renderButton()}
              </Content>

              <Content style={styles.socaialButtons}>
                <Text style={styles.centerTextStyle}> or </Text>
                <FacebookButton label='Signup with Facebook' auth={this.props.signup} />
                <TwitterButton label='Signup with Twitter' auth={this.props.signup} />
              </Content>
            </Content>
          </Image>
        </Container>
      );
  }
}

const mapStateToProps = ({ global }) => {
  const { error, loading } = global;
  return { error, loading };
};

const styles = {
  errorTextStyles: {
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 17,
    alignSelf: 'center',
    color: 'red'
  },
  backgroundImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: null
  },
  rightTextStyle: {
    alignSelf: 'flex-end',
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  centerTextStyle: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 25,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  socaialButtons: {
    paddingTop: 20,
    alignSelf: 'center'
  }
};
export default connect(mapStateToProps, { getSessionToken, signup, clearError })(SignUpForm);

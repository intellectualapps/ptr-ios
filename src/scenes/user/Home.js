import React, { Component } from 'react';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Card, CardItem, Left, Right,
  Body, H2, Text, Button, Icon } from 'native-base';
import { connect } from 'react-redux';
import { clearError } from '../../reducers/global/GlobalActions';
import { login } from '../../reducers/auth/AuthActions';
import * as styles from '../../config/styles';

class Home extends Component {
  state = { email: '', password: '' };

  renderUserStatus() {
    if (this.props.authData.isEmailAddressVerified) {
      return <Text note style={styles.pentorLabel}>Status: Email verified</Text>;
    }

    return <Text note style={styles.pentorLabel}>Status: Awaiting Email Verification</Text>;
  }
  render() {
      return (
        <Container>
          <Content>
            <Card >
              <CardItem>
                <Body>
                    <Text style={styles.pentorLabel}>Welcome</Text>
                    {this.renderUserStatus()}
                </Body>
              </CardItem>
            </Card>

            <Card>
              <CardItem>
                <Left />
                  <Image
                    source={require('../../images/profile-icon.png')}
                  />
                <Right />
              </CardItem>

              <CardItem content>
                <Button transparent onPress={() => Actions.basicdetails()}>
                  <H2 style={styles.pentorLabel}> Tap here to create a profile </H2>
                </Button>
              </CardItem>
              <CardItem>
                  <Left>
                      <Button transparent>
                          <Icon style={styles.pentorLabel} active name="mail" />
                          <Text style={styles.pentorLabel}>{this.props.authData.email}</Text>
                      </Button>
                  </Left>
            </CardItem>
           </Card>
          </Content>
        </Container>
      );
  }
}

const mapStateToProps = ({ auth }) => {
  const { authData } = auth;
  return { authData };
};

export default connect(mapStateToProps, { login, clearError })(Home);

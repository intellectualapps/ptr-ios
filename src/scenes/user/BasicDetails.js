import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Spinner, Input, Item, Card, CardItem,
  Label, Form, Text, Button, Switch, Left, Right, Body, Icon } from 'native-base';
import { connect } from 'react-redux';
import { login, clearError } from '../../reducers/auth/AuthActions';
import DatePicker from '../../components/modals/DatePicker';
import { user } from '../../models/User';
import * as styles from '../../config/styles';

class BasicDetails extends Component {
  state = {
    modalVisible: false,
    firstname: '',
    lastname: '',
    phonenumber: '',
    birthday: '01-01-1990',
    isMentor: false
  };

  componentWillMount() {
    const userModel = user.getBasicDetails();
    this.setState({
      firstname: userModel.firstname,
      lastname: userModel.lastname,
      phonenumber: userModel.phonenumber,
      birthday: userModel.birthday,
      isMentor: userModel.isMentor
    });
  }

  showNextPage() {
    user.setBasicDetails(this.state);
    if (this.state.isMentor === true) {
      Actions.interests();
    } else {
      Actions.profile();
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="small" />;
    }

    return (
      <Button info block large style={{ marginTop: 20 }} onPress={() => this.showNextPage()}>
        <Text> Next </Text>
      </Button>
    );
  }

  render() {
      return (
        <Container>
          <View style={{ flex: 5 }}>
            <Content >
              <Form>
                <Item inlineLabel >
                <Label style={styles.pentorLabel}> First Name </Label>
                  <Input
                    autoCorrect={false}
                    style={styles.pentorInput}
                    onChangeText={(val) => this.setState({ firstname: val })}
                    value={this.state.firstname}
                  />
                </Item>
                <Item inlineLabel >
                  <Label style={styles.pentorLabel}> Last Name </Label>
                  <Input
                    autoCorrect={false}
                    style={styles.pentorInput}
                    onChangeText={(val) => this.setState({ lastname: val })}
                    value={this.state.lastname}
                  />
                </Item>
                <Item inlineLabel >
                  <Label style={styles.pentorLabel}> Phone Number </Label>
                  <Input
                    autoCorrect={false}
                    style={styles.pentorInput}
                    onChangeText={(val) => this.setState({ phonenumber: val })}
                    value={this.state.phonenumber}
                  />
                </Item>
                <Card>
                  <CardItem
                    onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}
                  >
                  <Left>
                    <Text style={styles.pentorLabel}>Birthday</Text>
                  </Left>
                  <Body>
                      <Text style={styles.pentorLabel}>{this.state.birthday}</Text>
                  </Body>
                  <Right>
                      <Icon name="arrow-forward" />
                  </Right>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem>
                  <Left>
                    <Text style={styles.pentorLabel}>I am willing to be a mentor</Text>
                  </Left>
                  <Right>
                      <Switch
                        onValueChange={(value) => this.setState({ isMentor: value })}
                        value={this.state.isMentor}
                      />
                  </Right>
                  </CardItem>
                </Card>
              </Form>
            </Content>
          </View>

          <View style={{ flex: 1 }}>
            {this.renderButton()}
          </View>

          <DatePicker
            onDoneClicked={(date) => {
              this.setState({ birthday: date });
                this.setState({ modalVisible: false });
            }}
            date={this.state.birthday}
            visible={this.state.modalVisible}
            onCancel={() => {
              this.setState({ modalVisible: false });
            }}
          />
        </Container>
      );
  }
}

const mapStateToProps = ({ auth }) => {
  const { email, password, user, error, loading } = auth;
  return { email, password, user, error, loading };
};

export default connect(mapStateToProps, { login, clearError })(BasicDetails);

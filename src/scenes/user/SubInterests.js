import React, { Component } from 'react';
import { Modal, TouchableOpacity } from 'react-native';
import MultipleChoice from 'react-native-multiple-choice';
import { CardItem, Text } from 'native-base';


const Confirm = ({ selectedInterests, interests, visible, children, onDecline }) => {
  const { containerStyle, textStyle, cardItemStyle } = styles;
  return (
      <Modal
        visible={visible}
        transparent
        animationType="fade"
        onRequestClose={() => {}}
      >
        <TouchableOpacity
          style={containerStyle}
          activeOpacity={1}
          onPress={onDecline}
        >
          <CardItem style={cardItemStyle}>
            <Text style={textStyle}>{children}</Text>
          </CardItem>
          <CardItem style={cardItemStyle}>
            <MultipleChoice
              options={interests}
              selectedOptions={selectedInterests}
              maxSelectedOptions={interests.length}
            />
          </CardItem>
      </TouchableOpacity>
      </Modal>
  );
};

const styles = {
  cardItemStyle: {
    justifyContent: 'center'
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center'
  }
};

export default Confirm;

import React, { Component } from 'react';
import { View, Image, TouchableHighlight, TouchableOpacity,
  ImagePickerIOS } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Card, CardItem, Left, Right,
  Body, H2, Text, Button, Icon } from 'native-base';
import { connect } from 'react-redux';
import { clearError } from '../../reducers/global/GlobalActions';
import { login } from '../../reducers/auth/AuthActions';
import * as globalStyles from '../../config/styles';
import * as colors from '../../config/colors';
import TextInput from '../../components/modals/TextInput';


class Profile extends Component {
  watchID: ?number = null;

  state = {
    aboutYouVisible: false,
    detailedBioVisible: false,
    aboutYou: '',
    aboutYouCount: 140,
    detailedBio: '',
    detailedBioCount: 2000,
    initialPosition: 'unknown',
    lastPosition: 'unknown',
    image: 'https://goo.gl/images/5Caorl'
   };

   pickImage() {
    // openSelectDialog(config, successCallback, errorCallback);
    ImagePickerIOS.openSelectDialog({}, imageUri => {
      this.setState({ image: imageUri });
    }, error => alert(error));
  }


  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const initialPosition = JSON.stringify(position);
        this.setState({ initialPosition });
      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
    this.watchID = navigator.geolocation.watchPosition((position) => {
      const lastPosition = JSON.stringify(position);
      this.setState({ lastPosition });
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  renderUserStatus() {
    if (this.props.authData.isEmailAddressVerified) {
      return <Text note style={globalStyles.pentorLabel}>Status: Email verified</Text>;
    }

    return <Text note style={globalStyles.pentorLabel}>Status: Awaiting Email Verification</Text>;
  }

  render() {
      return (
        <Container>
          <View style={{ flex: 7 }}>
            <Content>
              <Card>
                <TouchableHighlight   style={{flex: 1 }} onPress={() => this.pickImage()}>
                  <Image
                    style={{ justifyContent: 'center', alignItems: 'center', resizeMode: 'cover', width: null, flex: 1, height: 300 }}
                    source={{ uri: this.state.image}}
                  >
                    <TouchableOpacity
                      style={{ backgroundColor: colors.PENTOR_BLUE, padding: 10 }}
                      onPress={() => this.pickImage()}
                    >
                      <Text style={{ color: 'white' }}> Add Photo </Text>
                    </TouchableOpacity>
                  </Image>
                </TouchableHighlight>
             </Card>

             <Card >
               <CardItem
                 onPress={() => this.setState({ aboutYouVisible: !this.state.aboutYouVisible })}
                 >
                 <Left>
                     <Text
                       note
                       style={{ ...globalStyles.pentorLabel, marginLeft: -1 }}
                     >
                        About You
                      </Text>
                 </Left>
                 <Right>
                   <Icon name="arrow-forward" />
                 </Right>
               </CardItem>
             </Card>

             <Card >
               <CardItem
                 onPress={() => this.setState({ detailedBioVisible: !this.state.detailedBioVisible })}
               >
                 <Left>
                     <Text
                       note
                       style={{ ...globalStyles.pentorLabel, marginLeft: -1 }}
                     >
                        Detailed Bio
                      </Text>
                 </Left>
                 <Right>
                   <Icon name="arrow-forward" />
                 </Right>
               </CardItem>
             </Card>

             <Card >
               <CardItem>
                 <Body>
                  <Text style={globalStyles.pentorLabel}>Location: {this.state.lastPosition}</Text>
                 </Body>
               </CardItem>
             </Card>
            </Content>
          </View>

          <View style={{ flex: 1 }}>
            <Button info block large >
                <Text style={{ color: 'white' }}> Submit </Text>
            </Button>
          </View>

          <TextInput
            rows={3}
            title='About You'
            count={this.state.aboutYouCount}
            onDoneClicked={(aboutYou) => {
              this.setState({ aboutYou });
              this.setState({ aboutYouVisible: false });
            }}
            text={this.state.aboutYou}
            visible={this.state.aboutYouVisible}
            onCancel={() => {
              this.setState({ aboutYouVisible: false });
            }}
          />

          <TextInput
            rows={10}
            title='Detailed Bio'
            count={this.state.detailedBioCount}
            onDoneClicked={(detailedBio) => {
              this.setState({ detailedBio });
              this.setState({ detailedBioVisible: false });
            }}
            text={this.state.detailedBio}
            visible={this.state.detailedBioVisible}
            onCancel={() => {
              this.setState({ detailedBioVisible: false });
            }}
          />
        </Container>
      );
  }
}

const mapStateToProps = ({ auth }) => {
  const { authData } = auth;
  return { authData };
};

export default connect(mapStateToProps, { login, clearError })(Profile);

import React, { Component } from 'react';
import { Image, View, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Spinner, Footer, FooterTab, Card, CardItem,
  Label, Form, H1, Text, Button, Switch, Left, Body, Right } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as styles from '../../config/styles';
import BackendFactory from '../../lib/BackendFactory';
import { appData } from '../../lib/AppData';
import SubInterests from './SubInterests';

class Interests extends Component {
    state = {
      interests: [],
      modalVisible: false,
      subInterests: ['a', 'b', 'c', 'd'],
      selectedSubInterests: []
    };

    componentWillMount() {
        // alert(JSON.stringify(this.props.basicdetails));
        this.getAreasOfInterests();
    }

    componentDidUpdate() {
      this.getAreasOfInterests();
    }

    getAreasOfInterests() {
      BackendFactory(appData.getAuthToken()).getAreasOfInterests()
          .then((result) => {
              this.setState({ interests: result.interests });
          }
      );
    }

    getSubInterests(id) {
      BackendFactory(appData.getAuthToken()).getSubInterests(id)
          .then((result) => {
            console.log(result);
              // this.setState({interests: result.interests});
              this.setState({ modalVisible: !this.state.modalVisible });
          }
      );
    }

    showNextPage() {
        Actions.login({ email, password });
    }

    updateSelectedInterests(interests){
      this.setState({ selectedSubInterests: interests });
      alert(JSON.stringify(this.state.selectedSubInterests));
    }
    renderInterests() {
      const interestsInGroupOfThree = [];
      while (this.state.interests.length > 0) {
        const chunk = this.state.interests.splice(0, 3);
        interestsInGroupOfThree.push(chunk);
      }

      return (
          <Grid>
          {interestsInGroupOfThree.map((interestGroup, i) => {
              return (<Row key={i}>
                  {interestGroup.map((interest, k) => {
                      return (<Col key={k} style={{ height: 150 }}>
                        <TouchableHighlight
                          style={{ flex: 1 }}
                          onPress={() => this.getSubInterests(interest.id)}
                        >
                          <Image
                            style={{ justifyContent: 'center',
                              alignItems: 'center',
                              resizeMode: 'cover',
                              width: null,
                              flex: 1,
                              height: 300 }}
                            source={{ uri: interest.imageLink }}

                          >
                            <Text
                              style={{ fontSize: 17, fontWeight: '900', color: 'white' }}
                            > {interest.name} </Text>
                          </Image>
                          </TouchableHighlight>
                      </Col>);
                  })}
              </Row>);
          })}
          </Grid>
        );
    }

    render() {
        return (
            <Container>
                <View style={{ flex: 5 }}>
                  <Content>
                    {this.renderInterests()}
                  </Content>
                </View>

                <View style={{ flex: 1 }}>
                  <Button
                    info block large
                    style={{ marginTop: 20 }}
                    onPress={() => Actions.profile()}
                  >
                      <Text style={{ color: 'white' }}> Next </Text>
                  </Button>
                </View>

                <SubInterests
                  updateSelectedInterests={() => this.updateSelectedInterests()}
                  selectedInterests={this.state.selectedSubInterests}
                  interests={this.state.subInterests}
                  visible={this.state.modalVisible}
                  onDecline={() => this.setState({ modalVisible: false })}
                >
                  Sub Interests
                </SubInterests>
            </Container>
        );
    }
}

export default Interests;

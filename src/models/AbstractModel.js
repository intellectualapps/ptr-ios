/**
 * # User.js
 *
 * User Model
 *
 */
'use strict';

import store from 'react-native-simple-store';

export default class AbstractModel {
  /**
   *
   * set storageKey in constructor
   */
  constructor(storageKey) {
    if (new.target === AbstractModel) {
      throw new TypeError("Cannot instantiate an Abstract Class");
    }

    this.storageKey = storageKey;
  }

  /**
   * ### storeModel
   * Store a model in data storage
   * @param {Array} model the currentUser object
   */
  storeModel(model) {
    return store.save(this.storageKey, model);
  }

  /**
   * ### getModel
   */
  getModel() {
    return store.get(this.storageKey);
  }
  /**
   * Delete model
   */
  deleteModel() {
    return store.delete(this.storageKey);
  }
}

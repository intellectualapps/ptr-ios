/**
 * # User.js
 * User Model
 */
'use strict';

import AbstractModel from './AbstractModel'

export class User extends AbstractModel {
  /**
   * set User
   */
  constructor() {
    super('user');
    this.firstname = "";
    this.lastname = "";
    this.phonenumber = "";
    this.birthday = "1990-01-01";
    this.isMentor = false;
  }

  setBasicDetails(basicdetails) {
    this.firstname = basicdetails.firstname;
    this.lastname = basicdetails.lastname;
    this.phonenumber = basicdetails.phonenumber;
    this.birthday = basicdetails.birthday;
    this.isMentor = basicdetails.isMentor;
  }

  getBasicDetails() {
    return ({
      firstname: this.firstname,
      lastname: this.lastname,
      phonenumber: this.phonenumber,
      birthday: this.birthday,
      isMentor: this.isMentor
    });
  }

}
// The singleton variable
export const user = new User();

import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';

const openDrawer = () => {
  Actions.refresh({
    key: 'drawer',
    open: true
  });
};

 export default {
   backButton() {
    return (
      <TouchableOpacity onPress={Actions.pop}>
        <Icon style={{ color: 'white' }} name='angle-left' />
      </TouchableOpacity>
    );
  },

  hamburgerButton() {
    return (
      <TouchableOpacity onPress={openDrawer}>
        <Icon style={{ color: 'white' }} active name='menu' />
      </TouchableOpacity>
    );
  },

  searchButton(callback: Function) {
    return (
      <TouchableOpacity onPress={callback}>
        <Icon style={{ color: 'white' }} active name='search' />
      </TouchableOpacity>
    );
  },
};

import React, { Component } from 'react';
import { View, Modal, TextInput } from 'react-native';
import { CardItem, Text, Button, Left, Right } from 'native-base';
import * as globalStyles from '../../config/styles';
import * as colors from '../../config/colors';


/*
@rows text box rows
@title Title or header
@visible visibility of the modal.. can be true or false
@count maximum word count
@text text to be displayed // TODO initialize to empty if null
@onDoneClicked callback function for done button
@onCancel callback function for cancel button
*/
class ModalTextInput extends Component {
  constructor(props) {
    super(props);
    if (props.text) {
      this.initialText = props.text;
    } else {
      this.initialText = '';
    }
    this.state = {
      text: this.initialText,
      count: props.count - this.initialText.length,
    };
  }

  render() {
    const { visible, onDoneClicked, onCancel } = this.props;
    const { containerStyle, cardItemStyle } = styles;

    const returnTextToParent = () => {
      onDoneClicked(this.state.text);
    };

    const cancelClicked = () => {
      this.setState({ text: this.initialText }, () => {
        onCancel();
      });
    };

    return (
        <Modal
          visible={visible}
          transparent
          animationType="slide"
          onRequestClose={() => {}}
        >
          <View style={containerStyle}>
            <View style={cardItemStyle}>
                <CardItem>
                  <Left>
                      <Text
                        note
                        style={{ ...globalStyles.pentorLabel, marginLeft: -1 }}
                      >
                        {this.props.title}
                      </Text>
                  </Left>
                  <Right>
                      <Text
                        note
                        style={globalStyles.pentorLabel}
                      >
                        {this.state.count}
                      </Text>
                  </Right>
                </CardItem>
                <CardItem>
                    <TextInput
                     style={styles.aboutyou}
                     onChangeText={(text) => {
                       this.setState({ text, count: this.props.count - text.length });
                     }}
                     value={this.state.text}
                     maxLength={this.props.count}
                     numberOfLines={this.props.rows}
                     multiline
                    />
                </CardItem>
            </View>

            <CardItem>
              <Button
                full info
                style={{ flex: 1, marginRight: 5 }}
                onPress={returnTextToParent}
              >
                <Text>Done</Text>
              </Button>
              <Button
                full info
                style={{ flex: 1, marginLeft: 5 }}
                onPress={cancelClicked}
              >
                <Text>Cancel</Text>
              </Button>
            </CardItem>
          </View>
        </Modal>
    );
  }
}

const styles = {
  cardItemStyle: {
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'flex-end'
  },
  aboutyou: {
    height: 300,
    flex: 1,
    fontSize: 15,
    color: colors.PENTOR_BLUE,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: -15
  }
};
export default ModalTextInput;

import React, { Component } from 'react';
import { View, Modal, DatePickerIOS } from 'react-native';
import { CardItem, Text, Button } from 'native-base';


/*
@visible visibility of the modal.. can be true or false
@date initial date to be displayed
@onDoneClicked callback function for done button
@onCancel callback function for cancel button
*/
class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: props.date
    };
  }

  render() {
    const { visible, onDoneClicked, onCancel } = this.props;
    const { containerStyle, cardItemStyle } = styles;

    const doSomethingWithDate = () => {
      const date = new Date(this.state.date);
      const formattedDdate = date.toLocaleDateString('en-GB');
      onDoneClicked(formattedDdate.replace(/\//g, '-'));
    };

    const cancelDateSelection = () => {
      this.setState({ date: this.props.date }, () => {
        onCancel();
      });
    };

    return (
        <Modal
          visible={visible}
          transparent
          animationType="slide"
          onRequestClose={() => {}}
        >
          <View style={containerStyle}>
            <View style={cardItemStyle}>
              <DatePickerIOS
                date={new Date(this.state.date)}
                minimumDate={new Date('1900-01-01')}
                maximumDate={new Date('2030-01-01')}
                mode="date"
                onDateChange={(date) => this.setState({ date: date.toLocaleDateString('en-GB') })}
              />
            </View>

            <CardItem>
              <Button
                full info
                style={{ flex: 1, marginRight: 5 }}
                onPress={doSomethingWithDate}
              >
                <Text>Done</Text>
              </Button>
              <Button
                full info
                style={{ flex: 1, marginLeft: 5 }}
                onPress={cancelDateSelection}
              >
                <Text>Cancel</Text>
              </Button>
            </CardItem>
          </View>
        </Modal>
    );
  }
}

const styles = {
  cardItemStyle: {
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'flex-end'
  }
};
export default DatePicker;

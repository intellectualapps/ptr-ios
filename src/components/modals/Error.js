import React, { Component } from 'react';
import { Image, View, Modal } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Content, Card, CardItem, Text, Button, Left, Right, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as globalstyles from '../../config/styles';
import BackendFactory from '../../lib/BackendFactory';
import { appData } from '../../lib/AppData';


const Confirm = ({ visible, children, onDecline}) => {
  const { containerStyle, textStyle, cardItemStyle} = styles
  return (
      <Modal
        visible={visible}
        transparent
        animationType="slide"
        onRequestClose={() => {}}
      >
        <View style={containerStyle}>
          <CardItem style={cardItemStyle}>
            <Text style={textStyle}>{children}</Text>
          </CardItem>

          <CardItem>
            <Button style={{flex: 1, marginLeft: 2, marginRight: 2}} block info onPress={onDecline}><Text>No</Text></Button>
          </CardItem>
        </View>
      </Modal>
  );
};

const styles = {
  cardItemStyle: {
    'justifyContent': 'center'
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center'
  }

}
export default Confirm;

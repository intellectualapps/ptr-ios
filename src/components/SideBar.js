import React, { Component } from 'react';
import { BackAndroid, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Container, Content, Card, CardItem, ListItem, Left,
  Body, Thumbnail, Right, Text, Icon } from 'native-base';
import { deleteAuthData } from './../reducers/auth/AuthActions';
import { appData } from '../lib/AppData';
import { getUser, prePopulateUserWithAuthData } from './../reducers/user/UserActions';


class SideBar extends Component {
  state = { displayName: this.displayName() };

  componentWillMount() {
    this.props.getUser().then((user) => {
      if (user.firstName !== '') {
        this.setState({ displayName: user.firstName + ' ' + user.lastName });
      }
    });
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (this.context.drawer.props.open) {
        this.toggleDrawer();
        return true;
      }
      return false;
    });
  }

  displayName() {
    if (appData.getFirstName() !== '') {
      return appData.getFirstName() + ' ' + appData.getLastName();
    }
    return appData.getEmail();
  }

  toggleDrawer() {
    this.context.drawer.toggle();
  }

  handleLogout = () => {
    Alert.alert(
      'Are you sure you want to logout?',
      null,
      [
        { text: 'YES', onPress: () => this.props.deleteAuthData() },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: true }
    );
    //this.toggleDrawer();
  }

  handlePressElavon = () => {
    this.toggleDrawer();
  }

  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem header>
              <Left>
                  <Thumbnail source={require('../images/profile-icon.png')} />
                  <Body>
                      <Text>{this.state.displayName}</Text>
                  </Body>
              </Left>
            </CardItem>
          </Card>

          <Card>
            <Card>
             <ListItem>
               <Left>
                 <Text> Manage Mentorship </Text>
               </Left>
               <Right>
                  <Icon name="arrow-forward" />
               </Right>
             </ListItem>

             <ListItem>
               <Left>
                 <Text> Find a Mentor </Text>
               </Left>
               <Right>
                  <Icon name="arrow-forward" />
               </Right>
             </ListItem>

             <ListItem>
               <Left>
                 <Text> My Profile </Text>
               </Left>
               <Right>
                  <Icon name="arrow-forward" />
               </Right>
             </ListItem>
             <ListItem onPress={() => this.handleLogout()} >
               <Left>
                 <Text> Logout </Text>
               </Left>
               <Right>
                  <Icon name="arrow-forward" />
               </Right>
             </ListItem>
           </Card>
         </Card>
        </Content>
      </Container>
    );
  }
}

SideBar.contextTypes = {
  drawer: React.PropTypes.object
};

const mapStateToProps = ({ user }) => {
  const { userData } = user;
  return { userData };
};

export default connect(mapStateToProps, { deleteAuthData, getUser, prePopulateUserWithAuthData })(SideBar);

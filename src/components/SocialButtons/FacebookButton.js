import React from 'react';
import { View } from 'react-native';
import { Button, Text, Icon } from 'native-base';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

const initUser = (token, auth) => {
  fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + token)
  .then((response) => response.json())
  .then((json) => {
    auth(json.email, 'na', token);
    })
  .catch(() => {
    alert('error while retrieving data from facebook graph');
  })
};

const facebookSignIn = (auth) => {
  LoginManager.logInWithReadPermissions(['email', 'public_profile']).then(
     (result) => {
      if (result.isCancelled) {
        alert('Login was cancelled');
      } else {
        // alert('Login was successful with permissions: '
          // + result.grantedPermissions.toString());

          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const accessToken = data.accessToken;
              initUser(accessToken, auth);
            }
          );
      }
    },
    (error) => {
       alert('Login failed with error: ' + error);
    }
  );
};

const FacebookButton = ({ auth, label }) => {
  return (
    <View style={{ paddingTop: 10 }}>
       <Button
         onPress={() => facebookSignIn(auth)}
       >
       <Icon active name='logo-facebook' />
        <Text> { label }</Text>
       </Button>
     </View>
  );
};

export default FacebookButton;

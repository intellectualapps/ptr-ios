import React from 'react';
import { NativeModules, View } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import { TWITTER_COMSUMER_KEY, TWITTER_CONSUMER_SECRET } from '../../config/constants';

const { TwitterSignin } = NativeModules;

function twitterSignIn(auth) {
  TwitterSignin.logIn(TWITTER_COMSUMER_KEY, TWITTER_CONSUMER_SECRET, (error, loginData) => {
    if (!error) {
      // alert(JSON.stringify(loginData));
      // auth(loginData.email, 'na', loginData.authToken);
      auth('badmusfaketwitter@gmail.com', 'na', loginData.authToken);
    } else {
      // alert(JSON.stringify(error));
    }
  });
}

const TwitterButton = ({ auth, label }) => {
  return (
    <View style={{ paddingTop: 10 }}>
       <Button info block onPress={() => twitterSignIn(auth)}>
         <Icon active name='logo-twitter' />
         <Text> { label }</Text>
       </Button>
     </View>
 );
};

export default TwitterButton;

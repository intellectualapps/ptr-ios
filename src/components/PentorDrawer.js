import React, { PropTypes, Component } from 'react';
import NavigationDrawer from 'react-native-drawer';
import { connect } from 'react-redux';
import { DefaultRenderer, Actions as NavigationActions } from 'react-native-router-flux';
import SideBar from './SideBar';

/* *******************
* Documentation: https://github.com/root-two/react-native-drawer
********************/

class Drawer extends Component {
  render() {
    const state = this.props.navigationState;
    const children = state.children;

    return (
      <NavigationDrawer
        ref='navigation'
        type='displace'
        open={state.open}
        onOpen={() => NavigationActions.refresh({ key: state.key, open: true })}
        onClose={() => NavigationActions.refresh({ key: state.key, open: false })}
        content={<SideBar />}
        tapToClose
        openDrawerOffset={0.2}
        panCloseMask={0.2}
        negotiatePan
        tweenHandler={(ratio) => ({
          main: { opacity: Math.max(0.54, 1 - ratio) }
        })}
      >
        <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
      </NavigationDrawer>
    );
  }
}

Drawer.propTypes = {
  navigationState: PropTypes.object,
  onNavigate: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);

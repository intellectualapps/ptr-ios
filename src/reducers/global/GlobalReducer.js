import * as C from './GlobalConstants';

const INITIAL_STATE = { loading: false, error: '' };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case C.START_ACTION:
      return {
        ...state,
        loading: true,
        error: '' };
    case C.ACTION_DONE:
      return {
        ...state,
        loading: false };
    case C.ERROR_MESSAGE:
      return {
        ...state,
        loading: false,
        error: action.payload };
    case C.CLEAR_ERROR:
      return {
        ...state,
        ...INITIAL_STATE };
    default:
      return state;
  }
};

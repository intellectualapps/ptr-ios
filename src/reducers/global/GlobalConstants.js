export const START_ACTION = 'START_ACTION';
export const ACTION_DONE = 'ACTION_DONE';
export const ERROR_MESSAGE = 'ERROR_MESSAGE';
export const CLEAR_ERROR = 'CLEAR_ERROR';

'use strict';


import * as C from './GlobalConstants';

export const clearError = () => {
  return {
    type: C.CLEAR_ERROR
  };
};

export const errorMessage = (dispatch, error) => {
  dispatch({
    type: C.ERROR_MESSAGE,
    payload: error });
};

export const startAction = (dispatch) => {
  dispatch({
    type: C.START_ACTION });
};

export const actionDone = (dispatch) => {
  dispatch({
    type: C.ACTION_DONE });
};

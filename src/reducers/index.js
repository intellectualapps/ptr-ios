import { combineReducers } from 'redux';
import GlobalReducer from './global/GlobalReducer';
import AuthReducer from './auth/AuthReducer';
import UserReducer from './user/UserReducer';

export default combineReducers({
  auth: AuthReducer,
  global: GlobalReducer,
  user: UserReducer
});

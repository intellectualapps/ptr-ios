'use strict';

import _ from 'underscore';
import { Actions } from 'react-native-router-flux';
import * as C from './AuthConstants';
import * as globalActions from '../global/GlobalActions';
import BackendFactory from '../../lib/BackendFactory';
import { appAuthData } from '../../lib/AppAuthData';
import { appData } from '../../lib/AppData';
import { validateEmail } from '../../lib/validations';

const loginUserSuccess = (dispatch, authData) => {
  globalActions.actionDone(dispatch);
  dispatch({
    type: C.LOGIN_USER_SUCCESS,
    payload: authData });
};

const signupUserSuccess = (dispatch, authData) => {
  globalActions.actionDone(dispatch);
  dispatch({
    type: C.SIGNUP_USER_SUCCESS,
    payload: authData });
};

const loginUserFailure = (dispatch, error) => {
  globalActions.errorMessage(dispatch, error);
};

const signupUserFailure = (dispatch, error) => {
  globalActions.errorMessage(dispatch, error);
};

/**
 * ## saveAuthData
 * @param {Object} response - to return to keep the promise chain
 * @param {Object} json - object with authData
 */
export function saveAuthData(json) {
  return appAuthData.storeAuthData(json);
}

/**
 * ## signup
 * @param {string} email - user's email
 * @param {string} password - user's password
 *
 * Call the server signup and if good, save the authData,
 * set the state to logout and signal success
 *
 * Otherwise, dispatch the error so the user can see
 */
export const signup = (email, password, socialToken = null) => {
  return (dispatch) => {
    // if email is valid proceed to signing up the user
    if (validateEmail(email)) {
      globalActions.startAction(dispatch);

      return BackendFactory().signup({ email, password }, socialToken)
      .then((json) => {
        if (typeof json.status !== 'undefined') {
          signupUserFailure(dispatch, json.message);
          return;
        }
        return saveAuthData(json)
          .then(() => {
            // instantiate authData
            appData.authData = json;
            // dispatch signup success action
            signupUserSuccess(dispatch, json);
            //dispatch(logoutState())
            // navigate to Home
            Actions.drawer();
            Actions.user();
            Actions.home();
          });
      })
      .catch((error) => {
        signupUserFailure(dispatch, error);
      });
    }

    // if email isn't valid this action will be dispatched
      globalActions.errorMessage(dispatch, 'Invalid email');
  };
};

/**
 * ## login
 * @param {string} email - user's email
 * @param {string} password - user's password
 *
 * Call the server login and if good, save the authData,
 * set the state to logout and signal success
 *
 * Otherwise, dispatch the error so the user can see
 */
export const login = (email, password, socialToken = null) => {
  return (dispatch) => {
    // if email is valid proceed to signing up the user
    if (validateEmail(email)) {
      globalActions.startAction(dispatch);

      return BackendFactory().login({ email, password }, socialToken)
      .then((json) => {
        if (json.hasOwnProperty('status')) {
          loginUserFailure(dispatch, json.message);
          return;
        }
        return saveAuthData(json)
          .then(() => {
            // instantiate authData
            appData.authData = json;

            // dispatch login success action
            loginUserSuccess(dispatch, json);
            //dispatch(logoutState())
            // navigate to Home
            Actions.drawer();
            Actions.user();
            Actions.home();
          });
      })
      .catch((error) => {
        loginUserFailure(dispatch, error);
      });
    }

  // if email isn't valid this action will be dispatched
    globalActions.errorMessage(dispatch, 'Invalid email');
  };
};

/**
 * ## AddAuthData action
 */
export function addAuthData(authData) {
  return {
    type: C.ADD_AUTH_DATA,
    payload: authData
  };
}

/**
 * ## DeleteToken actions
 */
export function deleteAuthDataRequestSuccess(dispatch) {
  globalActions.actionDone(dispatch);
  dispatch({
    type: C.DELETE_AUTH_DATA_SUCCESS
  });
}

/**
 * ## Delete authdata
 *
 * Call the AppAutData deleteAuthData
 */
export const deleteAuthData = () => {
  return dispatch => {
    globalActions.startAction(dispatch);
    return appAuthData.deleteAuthData()
      .then(() => {
        deleteAuthDataRequestSuccess(dispatch);
        Actions.login();
      });
  };
};

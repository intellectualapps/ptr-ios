import * as C from './AuthConstants';

const INITIAL_STATE = { authData: null };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case C.LOGIN_USER_SUCCESS:
    case C.SIGNUP_USER_SUCCESS:
    case C.ADD_AUTH_DATA:
      return {
        ...state,
        authData: action.payload,
     };
    case C.DELETE_AUTH_DATA_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE };
    default:
      return state;
  }
};

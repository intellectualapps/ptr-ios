'use strict';

import { Actions } from 'react-native-router-flux';
import * as C from './UserConstants';
import * as globalActions from '../global/GlobalActions';
import BackendFactory from '../../lib/BackendFactory';
import { appData } from '../../lib/AppData';

const updateUserSuccess = (dispatch, user) => {
  globalActions.actionDone(dispatch);
  dispatch({
    type: C.UPDATE_USER_SUCCESS,
    payload: user });
};

const updateUserFailure = (dispatch, error) => {
  globalActions.errorMessage(dispatch, error);
};

/**
 * ## login
 * @param {string} email - user's email
 * @param {string} password - user's password
 *
 * Call the server login and if good, save the sessionToken,
 * set the state to logout and signal success
 *
 * Otherwise, dispatch the error so the user can see
 */
export const updateUser = (email, password, token = null) => {
  return (dispatch) => {
    globalActions.startAction(dispatch);

    return BackendFactory().login({ email, password }, token)
    .then((json) => {
      if (json.hasOwnProperty('status')) {
        updateUserFailure(dispatch, json.message);
        return;
      }
        updateUserSuccess(dispatch, json);
        //dispatch(logoutState())
        // navigate to Home
        Actions.drawer();
        Actions.user();
        Actions.home();
    })
    .catch((error) => {
      updateUserFailure(dispatch, error);
    });
  };
};

export const getUser = (email = appData.getEmail()) => {
  return (dispatch) => {
    globalActions.startAction(dispatch);

    return BackendFactory(appData.getAuthToken()).getUser(email)
    .then((json) => {
      if (json.hasOwnProperty('status')) {
        updateUserFailure(dispatch, json.message);
        return;
      }
        updateUserSuccess(dispatch, json);

        // update appData
        appData.setFirstName(json.firstName);
        appData.setLastName(json.lastName);

        return new Promise((resolve) => {
            resolve(json);
          }
        );
    })
    .catch((error) => {
      updateUserFailure(dispatch, error);
    });
  };
};

import * as C from './UserConstants';

const INITIAL_STATE = { userData: null };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case C.UPDATE_USER_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        userData: action.payload
      };
    default:
      return state;
  }
};

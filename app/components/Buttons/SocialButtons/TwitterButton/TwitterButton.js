import React from 'react'
import {SocialIcon} from 'react-native-elements'

function signUpwithTwitter () {
      alert('TODO: implement Twitter Login')
}

const TwitterButton = () => {
  return (
    <SocialIcon
      title='Login With Twitter'
      button
      type='twitter'
      onPress={() => signUpwithTwitter()}
    />

  )
}

TwitterButton.propTypes = {
  onDetailsPress: React.PropTypes.func
}

export default TwitterButton

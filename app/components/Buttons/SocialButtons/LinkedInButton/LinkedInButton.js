import React from 'react'
import {SocialIcon} from 'react-native-elements'

function signUpwithLinkedIn () {
      alert('TODO: implement LinkedIn Login')
}

const LinkedInButton = () => {
  return (
    <SocialIcon
      title='Login With LinkedIn'
      button
      type='linkedin'
      onPress={() => signUpwithLinkedIn()}
    />

  )
}

LinkedInButton.propTypes = {
  onDetailsPress: React.PropTypes.func
}

export default LinkedInButton

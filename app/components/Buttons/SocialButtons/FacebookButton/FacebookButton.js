import React from 'react'
import {SocialIcon} from 'react-native-elements'
import {LoginManager} from 'react-native-fbsdk'

function signUpwithFaceBook () {
  LoginManager.logInWithReadPermissions(['public_profile']).then(
    function(result) {
      if (result.isCancelled) {
        alert('Login was cancelled')
      } else {
        alert('Login was successful with permissions: '
          + result.grantedPermissions.toString())
      }
    },
    function(error) {
      alert('Login failed with error: ' + error)
    }
  )
}

const FacebookButton = () => {
  return (
    <SocialIcon
      title='Login With Facebook'
      button
      type='facebook'
      onPress={() => signUpwithFaceBook()}
    />

  )
}

FacebookButton.propTypes = {
  onDetailsPress: React.PropTypes.func
}

export default FacebookButton

import React from 'react'
import { Text } from 'react-native'

const AuthText = ({ onPress, children }) => {
  return <Text style={styles.textStyle} onPress={onPress}> {children} </Text>
}

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  }
}

export { AuthText }

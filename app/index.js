/**
 * # reducers
 *
 * This class combines all the reducers into one
 *
 */
'use strict'
/**
 * ## Imports
 *
 * our reducers
 */

 /**
 * ## imports
 *
 */
/**
 * ### React
 *
 * Necessary components from ReactNative
 */
import React from 'react'
import {
    AppRegistry} from 'react-native'

/**
 * ### configureStore
 *
 *  ```configureStore``` will connect the ```reducers```, the
 *
 */
import configureStore from './lib/configureStore'
/**
 * ### Router-Flux
 *
 * Necessary components from Router-Flux
 */
import {
    Router,
    Scene} from 'react-native-router-flux'

/**
 * ### Redux
 *
 * ```Provider``` will tie the React-Native to the Redux store
 */
import {Provider} from 'react-redux'

import Register from './containers/Register'
import Login from './containers/Login'
import SplashScreen from './containers/splashscreen'
import Home from './containers/Home'
import Drawer from './containers/Drawer'
import NavigationItems from './components/NavigationItems'
import AuthInitialState from './reducers/auth/authInitialState'

import styles from './themes/ApplicationStyles'

/**
 *
 * ## Initial state
 * Create instances for the keys of each structure in snowflake
 * @returns {Object} object with 4 keys
 */
function getInitialState () {
  const _initState = {
    auth: new AuthInitialState()
    // device: (new DeviceInitialState()).set('isMobile', true),
    // global: (new GlobalInitialState()),
    // profile: new ProfileInitialState()
  }
  return _initState
}

export default function native () {
  let ptr = React.createClass({
    render () {
      const store = configureStore(getInitialState())

      return (
        <Provider store={store}>
          <Router >
            <Scene key="root" >
              <Scene key="SplashScreen" component={SplashScreen} title="SplashScreen" hideNavBar initial={true} />
              <Scene key="Register" component={Register} title="Register" hideNavBar/>
              <Scene key="Login" component={Login} title="Login" hideNavBar/>
              <Scene key='drawer' component={Drawer} open={false}>
                <Scene
                  key='drawerChildrenWrapper'
                  navigationBarStyle={styles.navBar}
                  titleStyle={styles.title}
                  leftButtonIconStyle={styles.leftButton}
                  rightButtonTextStyle={styles.rightButton}
                >
                  <Scene key="Home"
                      component={Home}
                      title="Pentor"
                      renderLeftButton={NavigationItems.hamburgerButton} />
                </Scene>
              </Scene>
            </Scene>
          </Router>
        </Provider>
      )
    }
  })
    /**
     * registerComponent to the AppRegistery and off we go....
     */

  AppRegistry.registerComponent('ptr', () => ptr)
}

const globalConstants = {
  SET_SESSION_TOKEN: 'SET_SESSION_TOKEN',
  SET_STORE: 'SET_STORE',
  SET_STATE: 'SET_STATE',
  GET_STATE: 'GET_STATE'
}

export default globalConstants

import auth from './auth'
import globalConstants from './global'

const constants = Object.assign(auth, barclays, elavon, globalConstants)

export default constants

export async function postRequest (url, params) {
  let formBody = []
  for (let property in params) {
    let encodedKey = encodeURIComponent(property)
    let encodedValue = encodeURIComponent(params[property])
    formBody.push(encodedKey + '=' + encodedValue)
  }
  formBody = formBody.join('&')

  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formBody
    })

    return await response.json()
  } catch (errors) {

  }
}

export async function getRequest (url) {
  try {
    let response = await fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    })

    return await response.json()
  } catch (errors) {

  }
}

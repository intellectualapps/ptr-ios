'use strict'

import C from '../../lib/constants/auth'
import BackendFactory from '../../lib/BackendFactory'
import {Actions} from 'react-native-router-flux'
import {appAuthToken} from '../../lib/AppAuthToken'

const _ = require('underscore')

/**
 * ## State actions
 * controls which form is displayed to the user
 * as in login, register, logout or reset password
 */

export function logoutState () {
  return {
    type: C.LOGOUT
  }
}
export function registerState () {
  return {
    type: C.REGISTER
  }
}

export function loginState () {
  return {
    type: C.LOGIN
  }
}

export function forgotPasswordState () {
  return {
    type: C.FORGOT_PASSWORD
  }
}

/**
 * ## Logout actions
 */
export function logoutRequest () {
  return {
    type: C.LOGOUT_REQUEST
  }
}

export function logoutSuccess () {
  return {
    type: C.LOGOUT_SUCCESS
  }
}
export function logoutFailure (error) {
  return {
    type: C.LOGOUT_FAILURE,
    payload: error
  }
}
/**
 * ## Login
 * After dispatching the logoutRequest, get the sessionToken
 *
 *
 * When the response is received and it's valid
 * change the state to register and finish the logout
 *
 * But if the call fails, like expired token or
 * no network connection, just send the failure
 *
 * And if you fail due to an invalid sessionToken, be sure
 * to delete it so the user can log in.
 *
 * How could there be an invalid sessionToken?  Maybe they
 * haven't used the app for a long time.  Or they used another
 * device and logged out there.
 */
export function logout () {
  return dispatch => {
    dispatch(logoutRequest())
    return appAuthToken.getSessionToken()

      .then((token) => {
        return BackendFactory(token).logout()
      })

      .then(() => {
        dispatch(loginState())
        dispatch(logoutSuccess())
        dispatch(deleteSessionToken())
        Actions.InitialLoginForm()
      })

      .catch((error) => {
        dispatch(loginState())
        dispatch(logoutFailure(error))
      })
  }
}
/**
 * ## onAuthFormFieldChange
 * Set the payload so the reducer can work on it
 */
export function onAuthFormFieldChange (field, value) {
  return {
    type: C.ON_AUTH_FORM_FIELD_CHANGE,
    payload: {field: field, value: value}
  }
}
/**
 * ## Signup actions
 */
export function signupRequest () {
  return {
    type: C.SIGNUP_REQUEST
  }
}
export function signupSuccess (json) {
  return {
    type: C.SIGNUP_SUCCESS,
    payload: json
  }
}
export function signupFailure (error) {
  return {
    type: C.SIGNUP_FAILURE,
    payload: error
  }
}
/**
 * ## SessionToken actions
 */
export function sessionTokenRequest () {
  return {
    type: C.SESSION_TOKEN_REQUEST
  }
}
export function sessionTokenRequestSuccess (token) {
  return {
    type: C.SESSION_TOKEN_SUCCESS,
    payload: token
  }
}
export function sessionTokenRequestFailure (error) {
  return {
    type: C.SESSION_TOKEN_FAILURE,
    payload: _.isUndefined(error) ? null : error
  }
}

/**
 * ## DeleteToken actions
 */
export function deleteTokenRequest () {
  return {
    type: C.DELETE_TOKEN_REQUEST
  }
}
export function deleteTokenRequestSuccess () {
  return {
    type: C.DELETE_TOKEN_SUCCESS
  }
}

/**
 * ## Delete session token
 *
 * Call the AppAuthToken deleteSessionToken
 */
export function deleteSessionToken () {
  return dispatch => {
    dispatch(deleteTokenRequest())
    return appAuthToken.deleteSessionToken()
      .then(() => {
        dispatch(deleteTokenRequestSuccess())
      })
  }
}
/**
 * ## Token
 * If AppAuthToken has the sessionToken, the user is logged in
 * so set the state to logout.
 * Otherwise, the user will default to the login in screen.
 */
export function getSessionToken () {
  return dispatch => {
    dispatch(sessionTokenRequest())
    return appAuthToken.getSessionToken()

      .then((token) => {
        if (token) {
          dispatch(sessionTokenRequestSuccess(token))
          dispatch(logoutState())
          Actions.Home()
        } else {
          dispatch(sessionTokenRequestFailure())
          Actions.InitialLoginForm()
        }
      })

      .catch((error) => {
        dispatch(sessionTokenRequestFailure(error))
        dispatch(loginState())
        Actions.InitialLoginForm()
      })
  }
}

/**
 * ## saveSessionToken
 * @param {Object} response - to return to keep the promise chain
 * @param {Object} json - object with sessionToken
 */
export function saveSessionToken (json) {
  return appAuthToken.storeSessionToken(json)
}
/**
 * ## signup
 * @param {string} email - user's email
 * @param {string} password - user's password
 *
 * Call the server signup and if good, save the sessionToken,
 * set the state to logout and signal success
 *
 * Otherwise, dispatch the error so the user can see
 */
export function signup (email, password) {
  return dispatch => {
    dispatch(signupRequest())
    return BackendFactory().signup({
      // username: username,
      email: email,
      password: password
    })

      .then((json) => {
        return saveSessionToken(
          Object.assign({}, json, {email: email})
          )
          .then(() => {
            dispatch(signupSuccess(
              Object.assign({}, json, {email: email})
            ))
            dispatch(logoutState())
            // navigate to Home
            Actions.Home()
          })
      })
      .catch((error) => {
        dispatch(signupFailure(error))
      })
  }
}

/**
 * ## Login actions
 */
export function loginRequest () {
  return {
    type: C.LOGIN_REQUEST
  }
}

export function loginSuccess (json) {
  return {
    type: C.LOGIN_SUCCESS,
    payload: json
  }
}

export function loginFailure (error) {
  return {
    type: C.LOGIN_FAILURE,
    payload: error
  }
}
/**
 * ## Login
 * @param {string} email - user's email
 * @param {string} password - user's password
 *
 * After calling Backend, if response is good, save the json
 * which is the currentUser which contains the sessionToken
 *
 * If successful, set the state to logout
 * otherwise, dispatch a failure
 */

export function login (email, password) {
  return dispatch => {
    dispatch(loginRequest())
    return BackendFactory().login({
      email: email,
      password: password
    })

      .then(function (json) {
        return saveSessionToken(json)
          .then(function () {
            dispatch(loginSuccess(json))
            // navigate to Home
            Actions.Home()
            dispatch(logoutState())
          })
      })
      .catch((error) => {
        dispatch(loginFailure(error))
      })
  }
}

/**
 * ## ResetPassword actions
 */
export function resetPasswordRequest () {
  return {
    type: C.RESET_PASSWORD_REQUEST
  }
}

export function resetPasswordSuccess () {
  return {
    type: C.RESET_PASSWORD_SUCCESS
  }
}

export function resetPasswordFailure (error) {
  return {
    type: C.RESET_PASSWORD_FAILURE,
    payload: error
  }
}
/**
 * ## ResetPassword
 *
 * @param {string} email - the email address to reset password
 * *Note* There's no feedback to the user whether the email
 * address is valid or not.
 *
 * This functionality depends on the server set
 * up correctly ie, that emails are verified.
 * With that enabled, an email can be sent w/ a
 * form for setting the new password.
 */
export function resetPassword (email) {
  return dispatch => {
    dispatch(resetPasswordRequest())
    return BackendFactory().resetPassword({
      email: email
    })
      .then(() => {
        dispatch(loginState())
        dispatch(resetPasswordSuccess())
        Actions.Login()
      })
      .catch((error) => {
        dispatch(resetPasswordFailure(error))
      })
  }
}

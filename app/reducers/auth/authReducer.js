/**
 * # authReducer.js
 *
 * The reducer for all the actions from the various log states
 */
'use strict'
/**
 * ## Imports
 * The InitialState for auth
 * fieldValidation for validating the fields
 * formValidation for setting the form's valid flag
 */
import InitialState from './authInitialState'
import fieldValidation from '../../lib/fieldValidation'
import formValidation from './authFormValidation'

/**
 * ## Auth actions
 */
import C from '../../lib/constants/auth'
const initialState = new InitialState()
/**
 * ## authReducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function authReducer (state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.mergeDeep(state)

  switch (action.type) {
    /**
     * ### Requests start
     * set the form to fetching and clear any errors
     */
    case C.SESSION_TOKEN_REQUEST:
    case C.SIGNUP_REQUEST:
    case C.LOGOUT_REQUEST:
    case C.LOGIN_REQUEST:
    case C.RESET_PASSWORD_REQUEST: {
      let nextState = state
        .setIn(['form', 'isFetching'], true)
        .setIn(['form', 'error'], null)
      return nextState
    }

    /**
     * ### Logout state
     * The logged in user logs out
     * Clear the form's error and all the fields
     */
    case C.LOGOUT:
      return formValidation(
      state.setIn(['form', 'state'], action.type)
        .setIn(['form', 'error'], null)
        // .setIn(['form', 'fields', 'username'], '')
        .setIn(['form', 'fields', 'email'], '')
        .setIn(['form', 'fields', 'password'], '')
        // .setIn(['form', 'fields', 'passwordAgain'], '')
    )

    /**
     * ### Loggin in state
     * The user isn't logged in, and needs to
     * login, register or reset password
     *
     * Set the form state and clear any errors
     */
    case C.LOGIN:
    case C.REGISTER:
    case C.FORGOT_PASSWORD:
      return formValidation(
      state.setIn(['form', 'state'], action.type)
        .setIn(['form', 'error'], null)
    )

    /**
     * ### Auth form field change
     *
     * Set the form's field with the value
     * Clear the forms error
     * Pass the fieldValidation results to the
     * the formValidation
     */
    case C.ON_AUTH_FORM_FIELD_CHANGE: {
      const {field, value} = action.payload
      let nextState = state.setIn(['form', 'fields', field], value)
          .setIn(['form', 'error'], null)

      return formValidation(
      fieldValidation(nextState, action)
      , action)
    }
    /**
     * ### Requests end, good or bad
     * Set the fetching flag so the forms will be enabled
     */
    case C.SESSION_TOKEN_SUCCESS:
    case C.SESSION_TOKEN_FAILURE:
    case C.SIGNUP_SUCCESS:
    case C.LOGIN_SUCCESS:
    case C.LOGOUT_SUCCESS:
    case C.RESET_PASSWORD_SUCCESS:
      return state.setIn(['form', 'isFetching'], false)

    /**
     *
     * The fetching is done, but save the error
     * for display to the user
     */
    case C.SIGNUP_FAILURE:
    case C.LOGOUT_FAILURE:
    case C.LOGIN_FAILURE:
    case C.RESET_PASSWORD_FAILURE:
      return state.setIn(['form', 'isFetching'], false)
      .setIn(['form', 'error'], action.payload)

    /**
     * ### Hot Loading support
     *
     * Set all the field values from the payload
     */
    case C.SET_STATE:
      var form = JSON.parse(action.payload).auth.form

      var next = state.setIn(['form', 'state'], form.state)
          .setIn(['form', 'disabled'], form.disabled)
          .setIn(['form', 'error'], form.error)
          .setIn(['form', 'isValid'], form.isValid)
          .setIn(['form', 'isFetching'], form.isFetching)
          // .setIn(['form', 'fields', 'username'], form.fields.username)
          // .setIn(['form', 'fields', 'usernameHasError'], form.fields.usernameHasError)
          .setIn(['form', 'fields', 'email'], form.fields.email)
          .setIn(['form', 'fields', 'emailHasError'], form.fields.emailHasError)
          .setIn(['form', 'fields', 'password'], form.fields.password)
          .setIn(['form', 'fields', 'passwordHasError'], form.fields.passwordHasError)
          // .setIn(['form', 'fields', 'passwordAgain'], form.fields.passwordAgain)
          // .setIn(['form', 'fields', 'passwordAgainHasError'], form.fields.passwordAgainHasError)

      return next

    case C.DELETE_TOKEN_REQUEST:
    case C.DELETE_TOKEN_SUCCESS:
        /**
         * no state change, just an ability to track action requests...
         */
      return state

  }
  /**
   * ## Default
   */
  return state
}

import React, { Component } from 'react'
import {View} from 'react-native'
import styles from './styles'
import Register from './Register'

export default class RegisterContainer extends Component {
  constructor () {
    super()

    this.state = {
      email: '',
      password: '',
      authToken: '',
      errors: []
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Register/>
      </View>
    )
  }
}

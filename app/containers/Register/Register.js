import React from 'react'
import {View, Text, Image} from 'react-native'
import { Button, AuthText, CardSection, Input, Spinner } from '../../components/common'
import { Actions } from 'react-native-router-flux'
import styles from './styles'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../../reducers/auth/authActions'

const Register = (props) => {
  function showLoginPage () {
    Actions.Login()
  }

  const renderButton = () => {
    if (props.loading) {
      return <Spinner size="large" />
    }

    return (
      <Button onPress={() => props.actions.signup('badmustaofeeq@gmail.com', 'password')}>
        Login
      </Button>
    )
  }

  return (
    <View style={styles.container}>
      <Image source={require('../../themes/images/register_bg.jpg')}
             style={styles.backgroundImage}>

        <View style={styles.login}>

        </View>

        <View style={styles.signup}>
                <Input
                  label="Email"
                  placeholder="email@domain.com"
                  onChangeText={(val) => this.setState({email: val})}
                  value={props.email}
                />

                <Input
                  secureTextEntry
                  label="password"
                  placeholder="password"
                  onChangeText={(val) => this.setState({password: val})}
                  value={props.password}
                />

              

              <CardSection>
                {renderButton()}
              </CardSection>
        </View>

        <View style={styles.socials}>
          <AuthText style={styles.textCenter}> or </AuthText>

        </View>
      </Image>
    </View>
  )
}

/**
 * ## Redux boilerplate
 */

function mapStateToProps (state) {
  return {
    auth: state.auth
    // global: state.global
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(authActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)

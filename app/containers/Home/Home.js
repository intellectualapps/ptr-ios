import React from 'react'
import { Text, View } from 'react-native'
import styles from './styles'

const Home = ({isOpen = false}) => {
  return (

    <View style={styles.container}>
      <Text style={styles.main}>
        Pentor
      </Text>
    </View>

  )
}

Home.propTypes = {
  onDetailsPress: React.PropTypes.func
}

export default Home

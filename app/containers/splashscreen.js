/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  View
} from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class SplashScreen extends Component {

  componentDidMount () {
    setTimeout(function () {
      Actions.Register()
    }, 3000)
  }
  render () {
    return (
      <View style={styles.container}>
        <Image source={require('../themes/images/logo.png')} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2a7fff'
  }
})

import React, { Component } from 'react'
import { ScrollView, BackAndroid } from 'react-native'
import styles from './styles'
import DrawerButton from '../../components/Buttons/DrawerButton'
import { Actions as NavigationActions } from 'react-native-router-flux'

class DrawerContent extends Component {

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (this.context.drawer.props.open) {
        this.toggleDrawer()
        return true
      }
      return false
    })
  }

  toggleDrawer () {
    this.context.drawer.toggle()
  }

  handlePressMerchants = () => {
    this.toggleDrawer()
    NavigationActions.Merchants()
  }

  handlePressElavon = () => {
    this.toggleDrawer()
    NavigationActions.Elavon()
  }

  handlePressBarclays = () => {
    this.toggleDrawer()
    NavigationActions.Barclays()
  }

  handlePressServiceOrders = () => {
    this.toggleDrawer()
    NavigationActions.ServiceOrders()
  }

  handlePressPushToRevolution = () => {
    this.toggleDrawer()
    // NavigationActions.deviceInfo()
  }

  render () {
    return (
        <ScrollView style={styles.container}>
          <DrawerButton text='Merchants' onPress={this.handlePressMerchants} />
          <DrawerButton text='Elavon' onPress={this.handlePressElavon} />
          <DrawerButton text='Barclays' onPress={this.handlePressBarclays} />
          <DrawerButton text='Service Orders' onPress={this.handlePressServiceOrders} />
          <DrawerButton text='Push to Revolution' onPress={this.handlePressPushToRevolution} />
        </ScrollView>
    )
  }
}

DrawerContent.contextTypes = {
  drawer: React.PropTypes.object
}

export default DrawerContent

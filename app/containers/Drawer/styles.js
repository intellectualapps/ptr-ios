import {Colors} from '../../themes/'

export default {
  drawer: {
    backgroundColor: Colors.background
  },
  main: {
    backgroundColor: Colors.clear
  }
}

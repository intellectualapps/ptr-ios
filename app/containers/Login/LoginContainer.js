import React, { Component } from 'react'
import {View} from 'react-native'
import styles from './styles'
import Login from './Login'

export default class LoginContainer extends Component {
  constructor () {
    super()

    this.state = {
      email: '',
      password: '',
      authToken: '',
      errors: []
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Login/>
      </View>
    )
  }
}

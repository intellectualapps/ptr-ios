export default {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
    flexDirection: 'column'
  },
  backgroundImage: {
    flex: 1,
    alignSelf: 'stretch',
    width: null
  },
  textCenter: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  textRight: {
    fontSize: 20,
    textAlign: 'right',
    margin: 10
  },
  signup: {
    flex: 3
    // alignItems: 'flex-start'
  },
  socials: {
    flex: 5
    // justifyContent: 'flex-end',
    // alignItems: 'flex-start'
  }
}

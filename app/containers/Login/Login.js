import React from 'react'
import {View, Text, Image} from 'react-native'
import {connect} from 'react-redux'
import { Button, Card, CardSection, Input, Spinner } from '../../components/common'
import { Actions } from 'react-native-router-flux'
import styles from './styles'
import * as authActions from '../../reducers/auth/authActions'

const Login = ({props}) => {
  function showLoginPage () {
    Actions.drawer()
    Actions.Home()
  }

  const renderButton = () => {
    if (this.props.loading) {
      return <Spinner size="large" />
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        Login
      </Button>
    )
  }

  return (
    <View style={styles.container}>
      <Image source={require('../../themes/images/register_bg.jpg')}
             style={styles.backgroundImage}>

        <View style={styles.login}>
          <Text style={[{color: 'white'}, styles.textRight]}
            onPress={() => showLoginPage()}>
              Login
          </Text>
        </View>

        <View style={styles.signup}>
          <CardSection>
                <Input
                  label="Email"
                  placeholder="email@domain.com"
                  onChangeText={(val) => this.setState({email: val})}
                  value={this.props.email}
                />
              </CardSection>

              <CardSection>
                <Input
                  secureTextEntry
                  label="password"
                  placeholder="password"
                  onChangeText={(val) => this.setState({password: val})}
                  value={this.props.password}
                />
              </CardSection>

              <Text style={styles.errorTextStyles}>
                {this.props.error}
              </Text>

              <CardSection>
                {renderButton()}
              </CardSection>
        </View>

        <View style={styles.socials}>
          <Text style={styles.textCenter}> or </Text>
        </View>
      </Image>
    </View>
  )
}

/**
 * ## Redux boilerplate
 */

function mapStateToProps (state) {
  return {
    auth: state.auth
    // global: state.global
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(authActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
